#include <sys/sendfile.h>
#include <sys/socket.h>
#include <sys/select.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <errno.h>
#include <fcntl.h>
//#include <poll.h>

#include <limits.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdarg.h>
#include <stdio.h>
#include <ctype.h>
#include <math.h>
#include <time.h>

#include <utility>
#include <vector>
#include <deque>
#include <map>


#define STANDALONE 1
#define STATLOGSIZE 80
#define STATLOGFILE 4096
#define SOCKETSEREBUFF 128
#define SAFETYINSPECTION 1

#define Print_line printf("LINE:%d syscall fail with errno:%d.\n",__LINE__,errno);
#if SAFETYINSPECTION == 1
#define Print_log(f_, ...) printf((f_), ##__VA_ARGS__);
#else
#define Print_log(f_, ...) ;
#endif

class WT_HT;
class StatLogDT;
class WorkParam;
class Router;

typedef std::vector<unsigned char> BodyDT;
typedef std::map<std::string,std::string> HttpHeadDT;
typedef void (*HandleURIFUNC)(WT_HT*,WorkParam*);

class HttpDT {
public:
    bool close_immediate,res_file;
    long bodysize,bodypos;
    char SP,CR,LF,HT;
    const int statfail=9999,statsucc=10000;
    std::string method,uri,version,field_name,field_value;
    int gstat,reqlinestat,headerstat,bodystat,tatleparse,buffsize,buffpos;
    std::string::const_iterator wr_iter;
    HttpHeadDT res_head,req_head;
    HttpHeadDT::iterator head_iter;
    char errmsg[128],*buff;

    int returncode,reshstat,stat;
    std::string httpversion1_1;

    BodyDT res_body,req_body;
    std::map<int,std::string> returncode_map;
    HttpDT() {
        SP = 32;
        CR = 13;
        LF = 10;
        HT = 9;
        parse_clear();
    }
    std::string& cppstringtolower(std::string &str) {
        for(char &c : str) {
            c = tolower(c);
        }
        return str;
    }

    inline bool is_tspecials(char t) __attribute__((always_inline)) {
        char const *tspecials = "()<>@,;:\\\"/[]?={} \t";
        for(int i=0; i<20; ++i) {
            if(t == tspecials[i]) {
                return true;
            }
        }
        return false;
    }
    inline bool is_token(char t) __attribute__((always_inline)) {
        if(t > 31 && t < 127 && ! is_tspecials(t)) {
            return true;
        }
        return false;
    }
    inline bool is_text(char t) __attribute__((always_inline)) {
        bool is = false;
        if(t > 31 && t != 127) {
            is = true;
        }
        return is;
    }
    inline bool is_fakeuri(char t) __attribute__((always_inline)) {
        if(t > 32 && t < 127) {
            return true;
        }
        return false;
    }
    inline bool is_digit(char t) {
        if(t >= '0' && t <= '9') {
            return true;
        }
        return false;
    }
    inline bool is_version(char t) {
        if(t == '/' || t == '.' || t == 'H' || t == 'T' || t == 'P' || is_digit(t)) {
            return true;
        }
        return false;
    }
    inline int nextchar() {
        if(tatleparse >= 8192) {
            returncode = 413;
            strncpy(errmsg,"Request Header Fields Too Large",32);
            Print_log("%.128s",errmsg);
            _seterror(false);
        }
        if(buffpos == buffsize) {
            return -1;
        }
        ++tatleparse;
        return buff[buffpos++];
    }
    void seterror(int t,int *substat) {
        sprintf(errmsg,"illegal char(%d) at gstat:%d substat:%d.\n",t,gstat,*substat);
        Print_log("%.128s",errmsg);
        *substat = statfail;
        _seterror(true);
    }
    void _seterror(bool close_immediate) {
        throw close_immediate;
    }
    int request_line() { //retrun 0 unfinish 1 success 2 fail
        int t;
        while(reqlinestat != statsucc && (t = nextchar()) > 0) {
            switch(reqlinestat) {
            case 0:
                if(t == HT) {
                    break;
                }
                if(is_token(t)) {
                    method += t;
                } else if(t == SP) {
                    reqlinestat = 1;
                } else {
                    seterror(t,&reqlinestat);
                }
                break;
            case 1:
                if(is_fakeuri(t)) {
                    uri += t;
                } else if(t == SP) {
                    reqlinestat = 2;
                } else {
                    seterror(t,&reqlinestat);
                }
                break;
            case 2:
                if(is_version(t)) {
                    version += t;
                } else if(t == CR) {
                    reqlinestat = 3;
                } else {
                    seterror(t,&reqlinestat);
                }
                break;
            case 3: //success
                if(t == LF) {
                    reqlinestat = statsucc;
                } else {
                    seterror(t,&reqlinestat);
                }
                break;
            default:
                sprintf(errmsg,"HttpDT::request_line illegal status %d.\n",reqlinestat);
                Print_log("%.128s",errmsg);
                _seterror(true);
                break;
            }
        }
        if(reqlinestat == statsucc) {
            return 1;
        } else if(t < 0) {
            return 0;
        } else {
            return 2;
        }
    }
    int p_header() { //retrun 0 unfinish 1 success 2 fail
        char t;
        while(headerstat != statsucc && (t = nextchar()) > 0) {
            switch(headerstat) {
            case 0:
                if(t == SP || t == HT) {
                    ;
                } else if(t == CR) {
                    headerstat = 4;
                } else if(is_token(t)) {
                    field_name += t;
                } else if(t == ':') {
                    headerstat = 1;
                } else {
                    seterror(t,&headerstat);
                }
                break;
            case 1:
                if(t == SP) {
                    ;
                } else if(is_text(t)) {
                    field_value += t;
                    headerstat = 2;
                } else {
                    seterror(t,&headerstat);
                }
                break;
            case 2:
                if(is_text(t)) {
                    field_value += t;
                } else if(t == CR) {
                    headerstat = 3;
                } else {
                    seterror(t,&headerstat);
                }
                break;
            case 3:
                if(t == LF) {
                    req_head[cppstringtolower(field_name)] = field_value;
                    field_name.clear();
                    field_value.clear();
                    headerstat = 0;
                } else {
                    seterror(t,&headerstat);
                }
                break;
            case 4: //success
                if(t == LF) {
                    headerstat = statsucc;
                } else {
                    seterror(t,&headerstat);
                }
                break;
            default:
                sprintf(errmsg,"HttpDT::p_header illegal status %d.\n",headerstat);
                Print_log("%.128s",errmsg);
                _seterror(true);
                break;
            }
        }
        if(headerstat == statsucc) {
            return 1;
        } else if(t < 0) {
            return 0;
        } else {
            return 2;
        }
    }
    int p_body() { //retrun 0 unfinish 1 success 2 fail
        char t;
        std::string content_length("Content-Length");
        if(bodystat == 0) {
            if(req_head.find(cppstringtolower(content_length)) != req_head.end()) {
                bodysize = std::stol(req_head[cppstringtolower(content_length)]);
                if(bodysize > 0) {
                    if(bodysize > 0x00ffffff) {
                        sprintf(errmsg,"HttpDT::p_body header to large.\n");
                        Print_log("%.128s",errmsg);
                        _seterror(true);
                    }
                    bodystat = 1;
                } else {
                    bodystat = statsucc;
                }
            } else {
                bodystat = statsucc;
            }
        }
        if(bodystat == statsucc) {
            return 1;
        }
        while(bodypos < bodysize) {
            if((t = nextchar()) > 0) {
                req_body.push_back(t);
                ++bodypos;
            } else {
                break;
            }
        }
        if(bodysize == bodypos) {
            bodystat = statsucc;
            return 1;
        } else if(t < 0) {
            return 0;
        } else {
            return 2;
        }
    }
    void parse_clear() {
        std::string returncode_reason;
        gstat = 0;
        reqlinestat = 0;
        headerstat = 0;
        bodystat = 0;
        bodypos = 0;
        bodysize = 0;
        tatleparse = 0;

        reshstat = 0;

        returncode = 0;
        close_immediate = false;
        req_body.clear();
        req_body.reserve(128);

        res_file = false;
        res_body.clear();
        res_body.reserve(128);

        method.clear();
        uri.clear();
        version.clear();
        method.shrink_to_fit();
        uri.shrink_to_fit();
        version.shrink_to_fit();

        field_name.clear();
        field_value.clear();
        field_name.shrink_to_fit();
        field_value.shrink_to_fit();

        req_head.clear();
        res_head.clear();

        httpversion1_1 = "HTTP/1.1";
        returncode_map[100] = returncode_reason.assign("Continue");
        returncode_map[101] = returncode_reason.assign("Switching Protocols");
        returncode_map[200] = returncode_reason.assign("OK");
        returncode_map[202] = returncode_reason.assign("Accepted");
        returncode_map[400] = returncode_reason.assign("Bad Request");
        returncode_map[404] = returncode_reason.assign("Not Found");
        returncode_map[426] = returncode_reason.assign("Upgrade Required");
        returncode_map[500] = returncode_reason.assign("Internal Server Error");
    }
    int parse_request(const char* const buff,int size) {
        //retrun 0 unfinish 1 success 2 fail
        int subparse_rlt = 1;
        this->buff = (char*)buff;
        buffsize = size;
        buffpos = 0;
        while(subparse_rlt == 1 && gstat != statsucc) {
            try {
                switch(gstat) {
                case 0:
                    subparse_rlt = request_line();
                    gstat = subparse_rlt == 1 ? 1 : 0;
                    break;
                case 1:
                    subparse_rlt = p_header();
                    gstat = subparse_rlt == 1 ? 2 : 1;
                    break;
                case 2:
                    subparse_rlt = p_body();
                    gstat = subparse_rlt == 1 ? statsucc : 2;
                    break;
                default:
                    Print_log("HttpDT::parse_request gstat %d not accept.\n",gstat);
                    break;
                }
            } catch(bool _close_immediate) {
                close_immediate = _close_immediate;
                subparse_rlt = 2;
                break;
            }

        }
        if(subparse_rlt == 1) {
            bodypos = 0;
            bodysize = -1;
            gstat = 0xf000;
        }
        return subparse_rlt;
    }
    void print_request(bool print_body = false) {
        printf("%s %s %s\n",method.c_str(),uri.c_str(),version.c_str());
        for(std::pair<std::string, std::string> ele : req_head) {
            printf("%s : %s\n",ele.first.c_str(),ele.second.c_str());
        }
        if(print_body) {
            for(unsigned char c : req_body) {
                printf("%c",c);
            }
            printf("\n");
        }
        return;
    }
    inline bool is_buff_full() {
        if(buffpos >= buffsize) {
            return true;
        }
        return false;
    }
    inline void buff_wrchar(char t) {
        buff[buffpos++] = t;
    }
    int response_line() { //retrun 0 unfinish 1 success 2 fail
        if(returncode_map.find(returncode) != returncode_map.end()) {
            buffpos = sprintf(buff,"HTTP/1.1 %d %s\r\n",returncode,(returncode_map[returncode]).c_str());
        } else {
            buffpos = sprintf(buff,"HTTP/1.1 %d %s\r\n",returncode,"No Reason");
        }
        if(buffpos > buffsize) {
            Print_line("response_line large then buffsize.\n");
        }
        return 1;
    }

    int w_header() { //retrun 0 unfinish 1 success 2 fail
        while(!is_buff_full() && reshstat != statsucc) {
            switch(reshstat) {
            case 0:
                head_iter = res_head.begin();
                wr_iter = (head_iter->first).cbegin();
                reshstat = 2;
                break;
            case 1:
                //abandon
                break;
            case 2:
                if(wr_iter != head_iter->first.cend()) {
                    buff_wrchar(*wr_iter);
                    wr_iter++;
                } else {
                    buff_wrchar(':');
                    wr_iter = head_iter->second.cbegin();
                    reshstat = 3;
                }
                break;
            case 3:
                if(wr_iter != head_iter->second.cend()) {
                    buff_wrchar(*wr_iter);
                    wr_iter++;
                } else {
                    buff_wrchar(CR);
                    reshstat = 4;
                }
                break;
            case 4:
                buff_wrchar(LF);
                head_iter++;
                if(head_iter != res_head.cend()) {
                    wr_iter = head_iter->first.cbegin();
                    reshstat = 2;
                } else {
                    reshstat = 5;
                }
                break;
            case 5:
                buff_wrchar(CR);
                reshstat = 6;
                break;
            case 6:
                buff_wrchar(LF);
                reshstat = statsucc;
                break;
            default:
                sprintf(errmsg,"HttpDT::w_header illegal status %d.\n",reshstat);
                Print_log("%.128s",errmsg);
            }
        }
        if(is_buff_full()) {
            return 0;
        } else if(reshstat == statsucc) {
            return 1;
        } else {
            return 2;
        }
    }
    void w_body_prepare(size_t size) {
        bodysize = size;
        res_file = true;
    }
    int w_body_fd(int r_fd,int w_fd,size_t size) {
        //The function is practiced by the caller
        //sendfile()
        return 1;
    }
    int w_body_BodyDT() {   //retrun 0 unfinish 1 success 2 fail
        while(!is_buff_full() && bodypos < bodysize) {
            buff_wrchar(res_body.at(bodypos++));
        }
        if(is_buff_full()) {
            return 0;
        }
        return 1;
    }
    int w_body() {
        if(res_file) {
            return w_body_fd(0,0,0);
        } else {
            return w_body_BodyDT();
        }
    }
    int w_response(char* buff,int size) {
        char int_to_cstr[12];
        int subparse_rlt = 1;
        std::string content_length("Content-Length");
        this->buff = (char*)buff;
        buffsize = size;
        buffpos = 0;

        if(bodysize == -1) {
            if(res_file) {
                Print_log("res_file == true need assign bodysize or call w_body_prepare().\n");
            } else {
                bodysize = res_body.size();
            }
        }
        if(res_head.find(content_length) == res_head.end()) {
            res_head[content_length] = std::to_string(bodysize);
        }

        while(subparse_rlt == 1 && gstat != statsucc) {
            switch(gstat) {
            case 0xf000:
                subparse_rlt = response_line();
                gstat = subparse_rlt == 1 ? 0xf001 : 0xf000;
                break;
            case 0xf001:
                subparse_rlt = w_header();
                gstat = subparse_rlt == 1 ? 0xf002 : 0xf001;
                break;
            case 0xf002:
                subparse_rlt = w_body();
                gstat = subparse_rlt == 1 ? statsucc : 0xf002;
                break;
            default:
                Print_log("HttpDT::parse_response gstat %x not accept.\n",gstat);
                break;
            }
        }
        return subparse_rlt;
    }
    void print_response(bool print_body = false) { //if res_file set will not print body
        if(returncode_map.find(returncode) != returncode_map.end()) {
            printf("HTTP/1.1 %d %s\r\n",returncode,(returncode_map[returncode]).c_str());
        } else {
            printf("HTTP/1.1 %d %s\r\n",returncode,"No Reason");
        }
        for(std::pair<std::string, std::string> ele : res_head) {
            printf("%s : %s\n",ele.first.c_str(),ele.second.c_str());
        }
        if(print_body) {
            for(unsigned char c : res_body) {
                printf("%c",c);
            }
            printf("\n");
        }
        return;
    }
};

class WebSocketDT {
public:
    short gstat;
    int word_count;
    std::string uri,identify;
    const short statsucc=10000;

    BodyDT r_data;
    bool r_fin,r_mask;
    unsigned long r_payload_len;
    unsigned char r_opcode;
    unsigned int r_masking_key;

    BodyDT s_data;
    bool s_fin,s_mask;
    unsigned long s_payload_len;  //will write by s_data.size()
    unsigned char s_opcode;
    unsigned int s_masking_key;

    pthread_mutex_t mutex_send;
    WebSocketDT() {
        gstat = 0;
        word_count = 0;
        r_claer();
        s_claer();
        pthread_mutex_init(&mutex_send,NULL);
    }
    void r_claer() {
        r_data.clear();
        r_fin = false;
        r_mask = false;
        r_payload_len = -1;
        r_opcode = 0;
        r_masking_key = 0;
    }
    void s_claer() {
        s_data.clear();
        s_fin = false;
        s_mask = false;
        s_payload_len = -1;
        s_opcode = 0;
        s_masking_key = 0;
    }
    bool Handshake() {
        return true;
    }
    int r_frame(const unsigned char * const buff,int buffsize) { //retrun 0 unfinish 1 success 2 fail
        int buffpos = 0;
        bool sub_rlt = true;
        while(sub_rlt && (buffpos < buffsize) && gstat != statsucc) {
            switch(gstat) {
            case 0:
                r_fin = (buff[buffpos] & 0x80) == 0 ? false : true;
                r_opcode = (buff[buffpos] & 0x0f);
                gstat = 1;
                ++buffpos;
                sub_rlt = true;
                break;
            case 1:
                word_count = 0;
                r_mask = (buff[buffpos] & 0x80) == 0 ? false : true;
                if ((buff[buffpos] & 0x7f) == 127) {
                    gstat = 2;
                } else if((buff[buffpos] & 0x7f) == 126) {
                    gstat = 3;
                } else {
                    gstat = 4;
                    r_payload_len = (buff[buffpos] & 0x7f);
                }
                ++buffpos;
                sub_rlt = true;
                break;
            case 2:
                r_payload_len = (r_payload_len << 8) + buff[buffpos];
                if(word_count < 8) {
                    gstat = 2;
                    ++word_count;
                } else {
                    gstat = 4;
                }
                ++buffpos;
                sub_rlt = true;
                break;
            case 3:
                r_payload_len = (r_payload_len << 8) + buff[buffpos];
                if(word_count < 1) {
                    gstat = 3;
                    ++word_count;
                } else {
                    gstat = 4;
                }
                ++buffpos;
                sub_rlt = true;
                break;
            case 4:
                if(r_mask) {
                    gstat = 5;
                    word_count = 0;
                } else {
                    gstat = 6;
                }
                sub_rlt = true;
                break;
            case 5:
                r_masking_key = (r_masking_key << 8) + buff[buffpos];
                if(word_count < 3) {
                    gstat = 5;
                    ++word_count;
                } else {
                    gstat = 6;
                }
                ++buffpos;
                sub_rlt = true;
                break;
            case 6:
                word_count = 0;
                if( r_payload_len == 0 ) {
                    gstat = statsucc;
                } else {
                    gstat = 7;
                }
                break;
            case 7:
                r_data.push_back(buff[buffpos]);
                if(word_count < r_payload_len -1 ) {
                    ++word_count;
                } else {
                    gstat = statsucc;
                }
                ++buffpos;
                sub_rlt = true;
                break;
            default:
                Print_log("WebSocketDT::r_frame gstat %x not accept.\n",gstat);
                break;
            }
        }
        if(!sub_rlt) {
            return 2;
        }
        if(gstat == statsucc) {
            return 1;
        }
        return 0;
    }
    int s_frame(unsigned char *buff,int buffsize) { //retrun 0 unfinish 1 success 2 fail
        int buffpos= 0,temp_shift;
        unsigned long temp_payload = 0;
        while(buffpos < buffsize && gstat != statsucc) {
            switch(gstat) {
            case 0x0f00:
                s_payload_len = s_data.size();
                buff[buffpos] = (s_fin ? 0x80 : 0x00) + (s_opcode & 0x0f);
                gstat = 0x0f01;
                ++buffpos;
                break;
            case 0x0f01:
                word_count = 0;
                if(s_payload_len < 126) {
                    temp_payload = s_payload_len;
                    gstat = 0x0f04;
                } else if(s_payload_len <= 0xffffffff) {
                    temp_payload = 126;
                    gstat = 0x0f02;
                } else {
                    temp_payload = 127;
                    gstat = 0x0f03;
                }
                buff[buffpos++] = (s_mask ? 0x80 : 0x00) + (0x8f & temp_payload);
                break;
            case 0x0f02:
                temp_shift = (8 * (1 - word_count));
                temp_payload = 0xff;
                buff[buffpos++] = ((temp_payload << temp_shift) & s_payload_len) >> temp_shift;
                if(word_count < 1) {
                    gstat = 0x0f02;
                    ++word_count;
                } else {
                    gstat = 0x0f04;
                }
                break;
            case 0x0f03:
                temp_shift = (8 * (7 - word_count));
                temp_payload = 0xff;
                buff[buffpos++] = ((temp_payload << temp_shift) & s_payload_len) >> temp_shift;
                if(word_count < 8) {
                    gstat = 0x0f03;
                    ++word_count;
                } else {
                    gstat = 0x0f04;
                }
                break;
            case 0x0f04:
                word_count = 0;
                if(s_mask) {
                    gstat = 0x0f06;
                } else {
                    gstat = 0x0f05;
                }
                break;
            case 0x0f05:
                if(word_count == 0) {
                    buff[buffpos++] = (0xff000000 & s_mask) >> 8*3;
                } else if(word_count == 1) {
                    buff[buffpos++] = (0x00ff0000 & s_mask) >> 8*2;
                } else if(word_count == 2) {
                    buff[buffpos++] = (0x0000ff00 & s_mask) >> 8*1;
                } else if(word_count == 3) {
                    buff[buffpos++] = (0x000000ff & s_mask);
                    gstat = 0x0f06;
                }
                ++word_count;
                break;
            case 0x0f06:
                word_count = 0;
                if(s_data.size() == 0) {
                    gstat = statsucc;
                } else {
                    gstat = 0x0f07;
                }
                break;
            case 0x0f07:
                buff[buffpos++] = s_data.at(word_count++);
                if(word_count > s_payload_len - 1) {
                    gstat = statsucc;
                }
                break;
            default:
                Print_log("WebSocketDT::s_frame gstat %x not accept.\n",gstat);
                break;
            }
        }
        if(gstat == statsucc) {
            return 1;
        }
        return 0;
    }
    void print_frame(bool print_data = false) {
        BodyDT *_data;
        bool _fin,_mask;
        unsigned long _payload_len;
        unsigned char _opcode;
        unsigned int _masking_key;

        _data = &r_data;
        _fin = r_fin;
        _mask = r_mask;
        _payload_len = r_payload_len;
        _opcode = r_opcode;
        _masking_key = r_masking_key;

        printf("fin:%c opcode:%x mask:%c payload_len:%lu ",_fin ? 'T' : 'F',_opcode,_mask ? 'T' : 'F',_payload_len);
        if(_mask) {
            printf("r_masking_key:%x ",_masking_key);
        }
        printf("\n");
        if(print_data) {
            for(unsigned char c : *(_data) ) {
                printf("%c",c);
            }
            printf("\n");
        }
        return;
    }
};

class StatLogDT {
public:
    char log[STATLOGSIZE];
};

class ConnParam {
public:
    time_t lastrstick;
    struct timeval recvto;
    struct sockaddr_in addr;
    int conn,recvbuffpos[2];
    unsigned char recvbuff[SOCKETSEREBUFF];
    std::deque<std::pair<std::string,bool>> *uriphase; //think more intelligent way to deliv uri parse info
    HttpDT htdt;
    WebSocketDT *wsdt;
    explicit ConnParam() {
        conn = 0;
        wsdt = nullptr;
        lastrstick = 0;
        recvto.tv_sec = 1;
        recvto.tv_usec = 0;
        recvbuffpos[0] = 0;
        recvbuffpos[1] = 0;
        uriphase = nullptr;
    }
    ~ConnParam() {
        if(conn > 0) {
            close(conn) == 0 ? 0 : Print_line;
        }
        if(wsdt != nullptr) {
            delete wsdt;
        }
    }
    char *addr_str() {
        static char buff[32];
        snprintf(buff,32,"%s:%d",inet_ntoa(addr.sin_addr),ntohs(addr.sin_port));
        return buff;
    }
    ConnParam(const ConnParam &f) = delete;
    const ConnParam& operator = (const ConnParam &other) = delete;
};

class WorkParam {
public:
    WT_HT *self;
    short gstat;
    time_t birthtick;
    bool is_work_loop;
    void *(WT_HT::*routine)(void*);

    pthread_t thread;
    pthread_attr_t thr_attr;

    ConnParam *cnpr;

    const static short stat_init = 0,stat_wait = 1,stat_ht = 2,stat_ws = 3,stat_err = 4,stat_destroy = 5;
    explicit WorkParam() {
        cnpr = nullptr;
        is_work_loop = true;
        birthtick = time(NULL);
        gstat = WorkParam::stat_init;
        pthread_attr_init(&(thr_attr));
        pthread_attr_setstacksize(&(thr_attr),PTHREAD_STACK_MIN) == 0 ? 0 : Print_line;
    }
    ~WorkParam() {;}
    WorkParam(const WorkParam &f) = delete;
    const WorkParam& operator = (const WorkParam &other) = delete;

    void assign(WT_HT *out_self,void *(WT_HT::*out_routine)(void*)) {
        self = out_self;
        routine = out_routine;
    }
};
struct Router_Node_DT {
public:
    short type;
    HandleURIFUNC handle,close_ws,handshake_ws;
    static const short ht=0,ws=1,ht_chunk=2,ws_chunk=3;
    explicit Router_Node_DT(short ty,HandleURIFUNC handle_or_handle_chunk,HandleURIFUNC o_handshake_ws = nullptr,HandleURIFUNC o_close_ws = nullptr) {
        type = ty;
        handle = handle_or_handle_chunk;
        if(ty == Router_Node_DT::ht || Router_Node_DT::ht_chunk) {
            close_ws = nullptr;
            handshake_ws = nullptr;
        } else {
            close_ws = o_close_ws == nullptr ? Router::close_ws : o_close_ws;
            handshake_ws = o_handshake_ws == nullptr ? Router::handshake_ws : o_handshake_ws;
        }
    }
};

struct Router_Node {
public:
    Router_Node *digital,*letter;
    std::map<std::string,Router_Node*> phase;
    Router_Node_DT *match,*final;

    explicit Router_Node(Router_Node_DT *_match = nullptr,Router_Node_DT *_final = nullptr) {
        if(_match == nullptr) {
            match = new Router_Node_DT(Router_Node_DT::ht,Router::not_found);
        } else {
            match = _match;
        }
        if(_final == nullptr) {
            final = new Router_Node_DT(Router_Node_DT::ht,Router::not_found);
        } else {
            final = _final;
        }
        letter = nullptr;
        digital = nullptr;
    }
    ~Router_Node() {
        if(letter != nullptr) {
            delete letter;
        }
        if(digital != nullptr) {
            delete digital;
        }
        for(const auto &x : phase) {
            delete x.second;
        }
        delete match;
        delete final;
    }
};

class Router {
public:
    Router_Node *root; //cannot be nullptr
    Router() {
        root = new Router_Node();
    }
    ~Router() {
        delete root;
    }
    static void add_data(ConnParam *cnpr) {
        char buff[48];
        time_t curr;
        std::string k,v;

        curr = time(NULL);
        strftime(buff,sizeof(buff),"%a, %d %b %Y %H:%M:%S GMT",gmtime (&curr));

        k.assign("Date");
        v.assign(buff);
        cnpr->htdt.res_head[k] = v;
    }
    inline static void send_same_name_file(ConnParam *cnpr,const char *filename) __attribute__((always_inline)) {
        int r_fd,r_cn;
        std::string k,v;
        unsigned char buff[64];
        cnpr->htdt.returncode = 200;

        k.assign("Content-Type");
        v.assign("text/html; charset=utf-8");
        cnpr->htdt.res_head[k] = v;
        Router::add_data(cnpr);

        r_fd = open(filename,0,O_RDONLY);
        if(r_fd < 0) {
            Print_line;
        }
        while((r_cn = read(r_fd,buff,sizeof(buff))) > 0) {
            for(int i = 0; i < r_cn; ++i) {
                cnpr->htdt.res_body.push_back(buff[i]);
            }
        }
        close(r_fd) == 0 ? 0 : Print_line;
    }
    static void send_uri_file(WT_HT *serv,WorkParam *wkpr) {
        std::string fpath((*wkpr->cnpr->uriphase).back()->first); //care empty uriphase
        send_same_name_file(wkpr->cnpr,fpath.c_str());
    }
    static void not_found(WT_HT *serv,WorkParam *wkpr) {
        char const *buff = "Not_Found";
        wkpr->cnpr->htdt.returncode = 404;
        for(int i = 0 ; buff[i] != '\0' ; ++i) {
            wkpr->cnpr->htdt.res_body.push_back(buff[i]);
        }
    }
    static void handle_ws(WT_HT *serv,WorkParam *wkpr) {
        std::string k;
        static int identify = 0; //require lock
        if(wkpr->cnpr->htdt.req_head.find(wkpr->cnpr->htdt.cppstringtolower(k.assign("Connection"))) != wkpr->cnpr->htdt.req_head.end()) {
            if(wkpr->cnpr->htdt.cppstringtolower(wkpr->cnpr->htdt.req_head[k]) == wkpr->cnpr->htdt.cppstringtolower(k.assign("Upgrade"))) {
                if(wkpr->cnpr->htdt.req_head.find(wkpr->cnpr->htdt.cppstringtolower(k.assign("Upgrade"))) != wkpr->cnpr->htdt.req_head.end()) {
                    if(wkpr->cnpr->htdt.cppstringtolower(wkpr->cnpr->htdt.req_head[k]) == wkpr->cnpr->htdt.cppstringtolower(k.assign("websocket"))) {
                        wkpr->cnpr->htdt.returncode = 101;
                        wkpr->cnpr->htdt.res_head[k.assign("Connection")] =  k.assign("Upgrade");
                        wkpr->cnpr->htdt.res_head[k.assign("Upgrade")] =  k.assign("websocket");

                        wkpr->cnpr->wsdt = new WebSocketDT();
                        wkpr->cnpr->wsdt->uri = wkpr->cnpr.htdt.uri;
                        wkpr->cnpr->wsdt->identify = std::to_string(identify);
                        return;
                    }
                }
            }
        }
        wkpr->cnpr->htdt.returncode = 426;
        return;
    }
    static void close_ws(WT_HT *serv,WorkParam *wkpr) {
        ;
    }
    inline void parse_uri(std::string &uri,std::deque<std::pair<std::string,bool>> &uriphase)__attribute__((always_inline)) {
        std::string phase;
        bool is_digital = true;
        for(size_t i=0; i<uri.size(); ++i) {
            if(uri.at(i) == '/') {
                if(phase.size() > 0) {
                    uriphase.push_back(std::make_pair(phase,is_digital));
                }
                phase.clear();
                is_digital = true;
            } else {
                phase += uri.at(i);
                if(is_digital && (uri.at(i) < '0' || uri.at(i) > '9')) {
                    is_digital = false;
                }
            }
        }
        return;
    }
    bool handler(WT_HT *serv,WorkParam *wkpr) {
        size_t i;
        Router_Node_DT *hnr;
        Router_Node *cur = root;
        bool is_ht = wkpr->cnpr->wsdt == nullptr;
        std::deque<std::pair<std::string,bool>> uriphase;
        parse_uri(is_ht ? wkpr->cnpr->htdt.uri : wkpr->cnpr->wsdt->uri,uriphase);
        for(i=0; i<uriphase.size(); ++i) {
            std::pair<std::string,bool> &p = uriphase.at(i);
            if(cur->phase.find(p.first) != cur->phase.end()) {
                cur = cur->phase.at(p.first).second;
            } else if(p.second && cur->digital != nullptr) {
                cur = cur->digital;
            } else if(cur->letter != nullptr) {
                cur = cur->letter;
            } else {
                break;
            }
        }
        if(uriphase.size() == i) {
            hnr = cur->match;
        } else {
            hnr = cur->final;
        }
        wkpr->cnpr->uriphase = &uriphase;
        if(is_ht) {
            if(hnr->type == Router_Node_DT::ht || hnr->type == Router_Node_DT::ht_chunk) {
                hnr->handle(serv,wkpr);
            } else if(hnr->type == Router_Node_DT::ws || hnr->type == Router_Node_DT::ws_chunk) {
                hnr->handshake_ws(serv,wkpr);
            } else {
                Print_log("Router::handler unknow Router_Node_DT::type %d.\n",hnr->type);
            }
        } else {
            hnr->handle(serv,wkpr);
        }
        wkpr->cnpr->uriphase = nullptr;
        return true;
    }
};

class WT_HT {
public:
    time_t tick;
    int listenfd,serv_port;
    struct sockaddr_in serv_addr;
    bool is_main_loop,allow_accppt;
    char serv_ip[16],working_directory[64],info_path[64],pidfilename[64],statfilename[64];

    pthread_mutex_t mutex_thr;
    std::deque<WorkParam*> destroy_thr;
    std::map<pthread_t,WorkParam*> survive_thr;

    pthread_mutex_t mutex_statlog;
    std::deque<StatLogDT *> statlog_use,statlog_idle;

    pthread_cond_t cond_conn;
    pthread_mutex_t mutex_conn;
    std::map<int,ConnParam*> attach_conn;
    std::deque<ConnParam*> destroy_conn,ready_conn,retire_conn_ht,retire_conn_ws;

    pthread_mutex_t mutex_reveal;
    std::map<std::string,std::string> serv_info;

    Router router;
    HttpHeadDT http_res_header;
    explicit WT_HT() {
        tick = 0;
        serv_port = 5000;
        is_main_loop = true;
        allow_accppt = true;
        strncpy(serv_ip,"127.0.0.1",16);

        strncpy(info_path,"/run/ws_ht_server/",63);
        strncpy(pidfilename,"ws_ht_server.pid",63);
        strncpy(statfilename,"ws_ht_server.stat",63);
        //shell mkdir /run/wt_ht_server/ before run
        std::string k,v;
        k.assign("Server");
        v.assign("WT_HT");
        http_res_header[k] = v;
        k.assign("Connection");
        v.assign("Keep-Alive");
        http_res_header[k] = v;
        k.assign("Keep-Alive");
        v.assign("timeout=180, max=32");
        http_res_header[k] = v;

        pthread_mutex_init(&mutex_thr,NULL) == 0 ? 0 : Print_line;
        pthread_mutex_init(&mutex_conn,NULL)== 0 ? 0 : Print_line;
        pthread_mutex_init(&mutex_reveal,NULL)== 0 ? 0 : Print_line;
        pthread_mutex_init(&mutex_statlog,NULL)== 0 ? 0 : Print_line;
        pthread_cond_init(&cond_conn,NULL)== 0 ? 0 : Print_line;
    }

    ~WT_HT() {
        pthread_cond_destroy(&cond_conn);
    }

    static inline void* pthread_adapter(void *ptr) __attribute__((always_inline)) {
        WorkParam *psrw = (WorkParam*)ptr;
        return ((psrw->self)->*(psrw->routine))(psrw);
    }

    int statlog_wr(const char * format, ...) {
        va_list args;
        bool statlogfull = false;
        StatLogDT *stlg = nullptr;

        pthread_mutex_lock(&mutex_statlog);
        if(statlog_use.size() > 108) {
            statlogfull = true;
        }
        pthread_mutex_unlock(&mutex_statlog);
        if(statlogfull) {
            return 0;
        }
        pthread_mutex_lock(&mutex_statlog);
        if(!statlog_idle.empty()) {
            stlg = statlog_idle.front();
            statlog_idle.pop_front();
        }
        pthread_mutex_unlock(&mutex_statlog);

        if(stlg == nullptr) {
            stlg = new StatLogDT();
        }
        va_start (args, format);
        vsnprintf (stlg->log,STATLOGSIZE,format, args);
        va_end (args);

        pthread_mutex_lock(&mutex_statlog);
        statlog_use.push_back(stlg);
        pthread_mutex_unlock(&mutex_statlog);
        return 1;
    }

    ssize_t http_body_sendfile(char *fpath,int conn) {
        int r_fd;
        size_t fsize;
        struct stat sb;
        stat(fpath,&sb) == 0 ? 0 : Print_line;
        if((sb.st_mode & S_IFMT) == S_IFREG || (sb.st_mode & S_IFMT) == S_IFLNK) {
            fsize = sb.st_size;
        }
        r_fd = open(fpath,0,O_RDONLY);
        if(r_fd < 0) {
            Print_line;
        }
        return sendfile(conn,r_fd,NULL,fsize);
    }

    inline bool dm_map_check(std::map<int,ConnParam*> *map,const ConnParam *cnpr,bool act,long thread,const char *func_name) { //if act true erase false insert
        bool doit = true;
        if(act) { //erase
#if SAFETYINSPECTION == 1
            if(map->find(cnpr->conn) != map->end()) {
                map->erase(cnpr->conn);
            } else {
                doit = false;
                Print_log("thread %lu erase no-exist cnpr(conn:%d cnpr:%p) at %s.\n",thread,cnpr->conn,cnpr,func_name);
            }
#else
            map->erase(wkpr->cnpr->conn);
#endif //SAFETYINSPECTION
        } else { //insert
#if SAFETYINSPECTION == 1
            if(map->find(cnpr->conn) == map->end()) {
                map->insert(std::make_pair(((ConnParam *)cnpr)->conn,((ConnParam *)cnpr)));
            } else {
                doit = false;
                Print_log("thread %lu insert duplication cnpr(conn:%d cnpr:%p) at %s.\n",thread,cnpr->conn,cnpr,func_name);
            }
#else
            map->insert(std::make_pair(((ConnParam *)cnpr),((ConnParam *)cnpr)));
#endif //SAFETYINSPECTION
        }
        return doit;
    }
    inline sosize work_http_check_send(int sockfd,const void *buf, size_t len) __attribute__((always_inline)) {
        int sosize;
        sosize = send(sockfd,buf,len,0);
        if(sosize == -1) {
            Print_line; //conn error
            throw 3;
        } else if(sosize != cnpr->htdt.buffpos * sizeof(char)) {
            statlog_wr("the connection was closed by the other party.\n");
            throw 2;
        }
        return sosize;
    }
    inline void work_http(WorkParam *wkpr) __attribute__((always_inline)) { //if throw 1 retire_conn 2 close by client 3 conn_error 4 protocol_error
        unsigned char sobuff[SOCKETSEREBUFF];
        int http_parse_rlt,sosize;

        ConnParam *(&cnpr) = wkpr->cnpr;
        setsockopt(cnpr->conn, SOL_SOCKET, SO_RCVTIMEO, (struct timeval *)&(cnpr->recvto), sizeof(cnpr->recvto));

        while((http_parse_rlt = cnpr->htdt.parse_request((const char* const)(cnpr->recvbuff+cnpr->recvbuffpos[0]),cnpr->recvbuffpos[1] - cnpr->recvbuffpos[0])) == 0) {
            cnpr->recvbuffpos[0] = 0;
            cnpr->recvbuffpos[1] = recv(cnpr->conn,cnpr->recvbuff,sizeof(cnpr->recvbuff),0);
            if(cnpr->recvbuffpos[1] == -1) {
                cnpr->recvbuffpos[1] = 0;
                if(errno == EAGAIN || errno == EWOULDBLOCK) {
                    statlog_wr("move conn(%p) to retireHTTP.",cnpr);
                    throw 2;
                }
                Print_line; //conn error
                throw 3;
            } else if(cnpr->recvbuffpos[1] == 0) {
                statlog_wr("the connection was closed by the other party.");
                throw 2;
            }
        }
        if(http_parse_rlt == 2) {
            if(cnpr->htdt.close_immediate) {
                statlog_wr("paese request fail:\n%s",cnpr->htdt.errmsg);
                throw 4;
            }
            //mark close
        }
        cnpr->recvbuffpos[0] = cnpr->htdt.buffpos == -1 ? sizeof(cnpr->recvbuff) : cnpr->htdt.buffpos;

        //cnpr->htdt.print_request();
        cnpr->htdt.res_head = http_res_header;
        router.handler(this,wkpr);
        while(cnpr->htdt.w_response((char*)sobuff,sizeof(sobuff)) == 0) {
            sosize = work_http_check_send(cnpr->conn,sobuff,cnpr->htdt.buffpos);
        }
        sosize = work_http_check_send(cnpr->conn,sobuff,cnpr->htdt.buffpos);
        cnpr->htdt.parse_clear();
        cnpr->lastrstick = tick;
    }
    void* work(void *workparam) {
        unsigned char sobuff[128];
        int recvsize,http_parse_rlt,ready;
        WorkParam *wkpr = (WorkParam*)workparam;
        const ConnParam **cnpr = (const ConnParam**)&(wkpr->cnpr);

        while(true) {
            if((!wkpr->is_work_loop) || (!is_main_loop)) {
                if(wkpr->cnpr != nullptr) {
                    pthread_mutex_lock(&mutex_conn);
                    dm_map_check(&attach_conn,wkpr->cnpr,true,wkpr->thread,__PRETTY_FUNCTION__); //erase attach_conn cnpr
                    ready_conn.push_back(wkpr->cnpr);
                    pthread_mutex_unlock(&mutex_conn);
                }
                *cnpr = nullptr;
                break;
            }
            if(*cnpr == nullptr) {
                pthread_mutex_lock(&mutex_conn);
                if(ready_conn.size() != 0) {
                    *cnpr = ready_conn.front();
                    ready_conn.pop_front();
                    dm_map_check(&attach_conn,*cnpr,false,wkpr->thread,__PRETTY_FUNCTION__); //insert attach_conn cnpr
                } else {
                    wkpr->gstat = WorkParam::stat_wait;
                    pthread_cond_wait(&cond_conn,&mutex_conn);
                }
                pthread_mutex_unlock(&mutex_conn);
                if(*cnpr == nullptr) {
                    continue;
                }
            }

            if(wkpr->cnpr->wsdt == nullptr) { //act as http
                wkpr->gstat = WorkParam::stat_ht;
                try {
                    work_http(wkpr);
                } catch(int err) { //if throw 1 retire_conn 2 close by client 3 conn_error 4 protocol_error
                    wkpr->gstat = WorkParam::stat_err;
                    if(err == 1) {
                        pthread_mutex_lock(&mutex_conn);
                        dm_map_check(&attach_conn,wkpr->cnpr,true,wkpr->thread,__PRETTY_FUNCTION__); //erase attach_conn cnpr
                        retire_conn_ht.push_back(wkpr->cnpr);
                        pthread_mutex_unlock(&mutex_conn);
                    } else if(err == 2 || err == 3) {
                        pthread_mutex_lock(&mutex_conn);
                        dm_map_check(&attach_conn,wkpr->cnpr,true,wkpr->thread,__PRETTY_FUNCTION__); //erase attach_conn cnpr
                        destroy_conn.push_back(wkpr->cnpr);
                        pthread_mutex_unlock(&mutex_conn);
                    } else if(err == 4) {
                        // send error message
                        pthread_mutex_lock(&mutex_conn);
                        dm_map_check(&attach_conn,wkpr->cnpr,true,wkpr->thread,__PRETTY_FUNCTION__); //erase attach_conn cnpr
                        destroy_conn.push_back(wkpr->cnpr);
                        pthread_mutex_unlock(&mutex_conn);
                    } else {
                        Print_log("WT_HT::work_http throw unknow err(%d).\n",err);
                    }
                    *cnpr = nullptr;
                }
            } else { //act as ws
                wkpr->gstat = WorkParam::stat_ws;
            }


        }
#if SAFETYINSPECTION
        if(wkpr->cnpr != nullptr) {
            Print_log("thread %lu exist but cnpr(%p) not nullptr.\n",wkpr->thread,wkpr->cnpr);
        }
#endif //SAFETYINSPECTION
        wkpr->gstat = WorkParam::stat_destroy;

        pthread_mutex_lock(&mutex_thr);
        survive_thr.erase(wkpr->thread);
        destroy_thr.push_back(wkpr);
        pthread_mutex_unlock(&mutex_thr);

        return NULL;
        /*
                ready  = poll(fds,1,3000);
                if(ready < 0) {
                    Print_line;
                    goto FINISH_CONN;
                } else if(ready == 0) { // recv timeout
                    goto FINISH_CONN;
                }
                if(fds[0].revents & (POLLIN | POLLPRI)) {
                    recvsize = recv(fds[0].fd,sobuff,sizeof(sobuff),0);
                    if(recvsize <= 0) {
                        Print_line;
                        goto FINISH_CONN;
                    }
                } else {
                    goto FINISH_CONN;
                }*/
    }
    void* retireHTTP(void *workparam) {
        int fdmax;
        fd_set rset,eset;
        struct timeval recvto;
        std::deque<ConnParam*> retire;
        std::deque<ConnParam*>::iterator iter;
        std::string rsh("retire_size_ht");

        while(is_main_loop) {
            fdmax = 0;
            FD_ZERO(&rset);
            FD_ZERO(&eset);
            recvto.tv_sec = 3;
            recvto.tv_usec = 0;
            pthread_mutex_lock(&mutex_conn);
            while(!retire_conn_ht.empty()) {
                retire.push_back(retire_conn_ht.front());
                retire_conn_ht.pop_front();
            }
            pthread_mutex_unlock(&mutex_conn);
            pthread_mutex_lock(&mutex_reveal);
            serv_info[rsh] = std::to_string(retire.size());
            pthread_mutex_unlock(&mutex_reveal);
            for(const auto x : retire) {
                FD_SET(x->conn,&rset);
                FD_SET(x->conn,&eset);
                fdmax = fdmax < x->conn ? x->conn : fdmax;
            }
            select(fdmax + 1,&rset,NULL,&eset,&recvto) > -1 ? 0 :Print_line;
            for(int i=0; i<retire.size(); ++i) {
                if(FD_ISSET((retire.at(i))->conn, &eset)) {
                    pthread_mutex_lock(&mutex_conn);
                    destroy_conn.push_back(retire.at(i));
                    pthread_mutex_unlock(&mutex_conn);
                    printf("retireHTTP remove except conn.\n");
                    goto DELRETIREELE;
                } else if(FD_ISSET((retire.at(i))->conn, &rset)) {
                    pthread_mutex_lock(&mutex_conn);
                    ready_conn.push_back(retire.at(i));
                    pthread_mutex_unlock(&mutex_conn);
                    pthread_cond_signal(&cond_conn) == 0 ? 0 : Print_line;
                    printf("retireHTTP conn to ready_conn.\n");
                    goto DELRETIREELE;
                } else if(difftime(tick,(retire.at(i))->lastrstick) > 180) {
                    pthread_mutex_lock(&mutex_conn);
                    destroy_conn.push_back(retire.at(i));
                    pthread_mutex_unlock(&mutex_conn);
                    printf("retireHTTP remove time out conn.\n");
                    goto DELRETIREELE;
                }
                if(false) {
DELRETIREELE:
                    iter = retire.begin() + i;
                    retire.erase(iter);
                }
            }
        }

        return NULL;
    }
    void* retireWS(void *workparam) {
        std::deque<ConnParam*> retire;
        std::string rsw("retire_size_ws");
        pthread_mutex_lock(&mutex_reveal);
        serv_info[rsw] = std::to_string(retire.size());
        pthread_mutex_unlock(&mutex_reveal);
        return NULL;
    }
    inline void recycle_serv_info(int &workflag) __attribute__((always_inline)) {
        int itmp[16];
        std::string stmp;

        pthread_mutex_lock(&mutex_reveal);
        stmp.assign("retire_size_ht");
        if(serv_info.find(stmp) == serv_info.end()) {
            serv_info[stmp] = std::to_string(0);
        }
        stmp.assign("retire_size_ws");
        if(serv_info.find(stmp) == serv_info.end()) {
            serv_info[stmp] = std::to_string(0);
        }
        pthread_mutex_unlock(&mutex_reveal);

        itmp[0] = itmp[1] = itmp[2] = itmp[3] = 0; //0 act  1 err 2 wait 3 destroy
        pthread_mutex_lock(&mutex_thr);
        for(std::pair<pthread_t,WorkParam*> i : survive_thr) {
            if((i.second)->gstat == WorkParam::stat_ht || (i.second)->gstat == WorkParam::stat_ws ||  (i.second)->gstat == WorkParam::stat_init) {
                ++itmp[0];
            } else if((i.second)->gstat == WorkParam::stat_err) {
                ++itmp[1];
            } else if((i.second)->gstat == WorkParam::stat_wait) {
                ++itmp[2];
            }
        }
        itmp[3] = destroy_thr.size();
        pthread_mutex_unlock(&mutex_thr);

        pthread_mutex_lock(&mutex_reveal);
        serv_info[stmp.assign("thr_cn_act")] = std::to_string(itmp[0]);
        serv_info[stmp.assign("thr_cn_err")] = std::to_string(itmp[1]);
        serv_info[stmp.assign("thr_cn_wait")] = std::to_string(itmp[2]);
        serv_info[stmp.assign("thr_cn_destroy")] = std::to_string(itmp[3]);
        pthread_mutex_unlock(&mutex_reveal);

        pthread_mutex_lock(&mutex_conn);
        itmp[0] = ready_conn.size();
        itmp[1] = destroy_conn.size();
        itmp[2] = retire_conn_ht.size();
        itmp[3] = retire_conn_ws.size();
        pthread_mutex_unlock(&mutex_conn);
        pthread_mutex_lock(&mutex_reveal);
        serv_info[stmp.assign("conn_size_ready")] = std::to_string(itmp[0]);
        serv_info[stmp.assign("conn_size_destroy")] = std::to_string(itmp[1]);
        serv_info[stmp.assign("conn_size_retire_ht")] = std::to_string(itmp[2]);
        serv_info[stmp.assign("conn_size_retire_ws")] = std::to_string(itmp[3]);
        pthread_mutex_unlock(&mutex_reveal);

        pthread_mutex_lock(&mutex_statlog);
        itmp[0] = statlog_use.size();
        itmp[1] = statlog_idle.size();
        pthread_mutex_unlock(&mutex_statlog);
        pthread_mutex_lock(&mutex_reveal);
        serv_info[stmp.assign("statlog_size_use")] = std::to_string(itmp[0]);
        serv_info[stmp.assign("statlog_size_idle")] = std::to_string(itmp[1]);
        pthread_mutex_unlock(&mutex_reveal);
    }
    inline int recycle_serv_admin(int &workflag) __attribute__((always_inline)) {
        std::string stmp;
        WorkParam *wkpr = nullptr;
        const short thread_unit = 8;
        std::map<double,WorkParam*> order;
        int itmp[4],thrcreate = 0,thrremove = 0;

        pthread_mutex_lock(&mutex_reveal);
        itmp[0] = std::stoi(serv_info[stmp.assign("conn_size_ready")]) +
                  std::stoi(serv_info[stmp.assign("conn_size_destroy")]) +
                  std::stoi(serv_info[stmp.assign("conn_size_retire_ht")]) +
                  std::stoi(serv_info[stmp.assign("conn_size_retire_ws")]) +
                  std::stoi(serv_info[stmp.assign("retire_size_ht")]) +
                  std::stoi(serv_info[stmp.assign("retire_size_ws")]);
        pthread_mutex_unlock(&mutex_reveal);
        allow_accppt = itmp[0] > 1000 ? false : true;

        pthread_mutex_lock(&mutex_reveal);
        itmp[0] = std::stoi(serv_info[stmp.assign("thr_cn_act")]) + std::stoi(serv_info[stmp.assign("thr_cn_err")]);
        itmp[1] = std::stoi(serv_info[stmp.assign("thr_cn_wait")]);
        itmp[2] = std::stoi(serv_info[stmp.assign("conn_size_ready")]);
        //0 thrsurvive 1 thrwait 2 coready
        pthread_mutex_unlock(&mutex_reveal);
        if(itmp[0] + itmp[1] < thread_unit) {
            thrcreate = thread_unit - (itmp[0] + itmp[1]);
        } else if(itmp[1] < itmp[2] && itmp[2] > 12) {
            thrcreate = ceil(((double)itmp[2] / thread_unit) / log2(itmp[0] + itmp[1])) * thread_unit;
            thrcreate = thrcreate > 40 ? log2(thrcreate - 39) + 39 : thrcreate;
            printf("thread create %d.\n",thrcreate);
        } else if(itmp[1] > 8 && itmp[1] > itmp[2]) {
            thrremove = sqrt(itmp[1] - itmp[2] + 1);
        } else {
            workflag+=6;
        }

        for(short i = 0; i<thrcreate; ++i) {
            wkpr = new WorkParam();
            wkpr->assign(this,&WT_HT::work);
            pthread_create(&(wkpr->thread),&(wkpr->thr_attr),&WT_HT::pthread_adapter,(void*)wkpr) == 0 ? 0 :Print_line;
            pthread_mutex_lock(&mutex_thr);
            survive_thr[wkpr->thread] = wkpr;
            pthread_mutex_unlock(&mutex_thr);
        }
        if(thrremove > 0) {
            pthread_mutex_lock(&mutex_thr);
            for(auto const &x : survive_thr) {
                if(order.size() <= thrremove) {
                    order[difftime(tick,x.second->birthtick)] = x.second;
                } else if(difftime(tick,x.second->birthtick) > order.rbegin()->first) {
                    order.erase(order.begin());
                    order[difftime(tick,x.second->birthtick)] = x.second;
                }
            }
            for(auto const &x : order) {
                survive_thr[x.second->thread]->is_work_loop = false;
            }
            pthread_mutex_unlock(&mutex_thr);
        }
        if(itmp[1] != 0 || thrremove != 0) {
            pthread_cond_broadcast(&cond_conn) == 0 ? 0 : Print_line;
        }
        return thrcreate + thrremove;
    }

    inline void recycle_wkpr_cnpr(int &workflag) __attribute__((always_inline)) {
        ConnParam *cnpr;
        WorkParam *wkpr = nullptr;
        pthread_mutex_lock(&mutex_thr);
        if(!destroy_thr.empty()) {
            wkpr = destroy_thr.front();
            destroy_thr.pop_front();
        }
        pthread_mutex_unlock(&mutex_thr);
        if(wkpr != nullptr) {
            Print_log("recycle pthread:%lu.\n",wkpr->thread);
            pthread_join(wkpr->thread,NULL) == 0 ? 0 : Print_line;
            delete wkpr;
        } else {
            workflag+=3;
        }
        for(int i=0; i<5; ++i) {
            cnpr = nullptr;
            pthread_mutex_lock(&mutex_conn);
            if(!destroy_conn.empty()) {
                cnpr = destroy_conn.front();
                destroy_conn.pop_front();
            }
            pthread_mutex_unlock(&mutex_conn);
            if(cnpr != nullptr) {
                delete cnpr;
            } else {
                ++workflag;
            }
        }
    }
    inline int recycle_statlog_head(char *stat_start,int &workflag) {
        char t,prefix;
        bool next_line;
        int tmpln,statln = 0;

        pthread_mutex_lock(&mutex_reveal);
        t = serv_info.begin()->first[0];
        for(auto const &x : serv_info) {
            if(t != x.first[0]) {
                next_line = true;
                t = x.first[0];
            } else {
                next_line = false;
            }
            tmpln = sprintf((stat_start+statln),"%s%s:%5s",next_line ? ".\n" : " ",x.first.c_str(),x.second.c_str());
            if(tmpln < 0) {
                Print_line;
            } else if(statln + tmpln > STATLOGFILE) {
                Print_line;
            }
            statln+=tmpln;
        }
        tmpln = sprintf((stat_start+statln),"\n");
        if(tmpln < 0) {
            Print_line;
        } else if(statln + tmpln > STATLOGFILE) {
            Print_line;
        }
        statln += tmpln;
        pthread_mutex_unlock(&mutex_reveal);
        return statln;
    }
    inline int recycle_statlog_log(char *stat_start,int statln,int statlog,int &workflag) {
        bool logfull;
        StatLogDT *stlg;
        const static short LogPerRun = 5;
        for(int lg=0; lg<LogPerRun; ++lg) {
            stlg = nullptr;
            logfull = false;
            pthread_mutex_lock(&mutex_statlog);
            if(! statlog_use.empty() ) {
                stlg = statlog_use.front();
                statlog_use.pop_front();
            }
            pthread_mutex_unlock(&mutex_statlog);
            if(stlg == nullptr) {
                workflag += (LogPerRun - lg);
                break;
            }
            if((statln+statlog)+STATLOGSIZE > STATLOGFILE) {
                statlog = statln;
            }
            statlog += snprintf(stat_start+(statln+statlog),STATLOGSIZE,"%s\n",stlg->log);
            printf("%s\n",stlg->log);
            pthread_mutex_lock(&mutex_statlog);
            if(statlog_idle.size() < 32) {
                statlog_idle.push_back(stlg);
            } else {
                logfull = true;
            }
            pthread_mutex_unlock(&mutex_statlog);
            if(logfull) {
                delete stlg;
            }
        }
        return statlog;
    }

    void *recycle(void *nil) {
        FILE *pidFI;
        int pidfd,statfd,statln,statlog,workflag; //If the program does not execute what I guess, workflag will increase.
        char stat_buff[128],*stat_start;

        strncpy(stat_buff,info_path,63) != NULL ? 0 : Print_line;
        pidfd = open(strncat(stat_buff,pidfilename,63), O_CREAT | O_WRONLY, 0664);
        if(pidfd < 0) {
            Print_line;
        }
        pidFI = fdopen(pidfd,"w");
        if(pidFI == NULL) {
            Print_line;
        }
        fprintf(pidFI,"%d",getpid()) > 0 ? 0 : Print_line;
        fclose(pidFI) == 0 ? 0 : Print_line;

        strncpy(stat_buff,info_path,63) != NULL ? 0 : Print_line;
        statfd = open(strncat(stat_buff,statfilename,63), O_CREAT | O_RDWR | O_TRUNC, 0664);
        ftruncate(statfd,STATLOGFILE) == 0 ? 0 : Print_line;
        stat_start =(char*) mmap(NULL,STATLOGFILE,PROT_READ|PROT_WRITE,MAP_SHARED,statfd,0);
        if(stat_start == MAP_FAILED) {
            Print_line;
        }

        statlog = statln = 0;
        while(is_main_loop) {
            workflag = 0;

            if(difftime(time(NULL),tick) > 1.0) {
                recycle_serv_info(workflag);
                recycle_serv_admin(workflag);
                statln = recycle_statlog_head(stat_start,workflag);
                tick = time(NULL);
            } else {
                workflag+=5;
            }
            recycle_wkpr_cnpr(workflag);
            statlog = recycle_statlog_log(stat_start,statln,statlog,workflag);

            if(workflag > 0) {
                usleep(10000*workflag) == 0 ? 0 : Print_line;
            }
        }

        munmap((void*)stat_start,STATLOGFILE) == 0 ? 0 : Print_line;
        close(statfd) == 0 ? 0 :Print_line;
        return NULL;
    }

    void listening() {
        ConnParam *cnpr;
        size_t sockaddr_size;
        WorkParam wkpr_recycle,wkpr_retireHTTP,wkpr_retireWS;

        memset(&serv_addr, '0', sizeof(serv_addr));
        serv_addr.sin_family = AF_INET;
        serv_addr.sin_port = htons(serv_port);
        serv_addr.sin_addr.s_addr = inet_addr(serv_ip);

        listenfd = socket(AF_INET, SOCK_STREAM, 0);
        if(listenfd < 0) {
            Print_line;
        }
        if(bind(listenfd, (struct sockaddr*)&serv_addr, sizeof(serv_addr)) == -1) {
            Print_line;
        }
        if(listen(listenfd, 10) == -1) {
            Print_line;
        }

        wkpr_recycle.assign(this,&WT_HT::recycle);
        pthread_create(&(wkpr_recycle.thread),&(wkpr_recycle.thr_attr),&WT_HT::pthread_adapter,(void*)&wkpr_recycle) == 0 ? 0 :Print_line;

        wkpr_retireHTTP.assign(this,&WT_HT::retireHTTP);
        pthread_create(&(wkpr_retireHTTP.thread),&(wkpr_retireHTTP.thr_attr),&WT_HT::pthread_adapter,(void*)&wkpr_retireHTTP) == 0 ? 0 :Print_line;

        wkpr_retireWS.assign(this,&WT_HT::retireWS);
        pthread_create(&(wkpr_retireWS.thread),&(wkpr_retireWS.thr_attr),&WT_HT::pthread_adapter,(void*)&wkpr_retireWS) == 0 ? 0 :Print_line;

        sockaddr_size = sizeof(cnpr->addr);
        Print_log("Server listen on %s:%d.\n",serv_ip,serv_port);
        while(is_main_loop) {
            cnpr = new ConnParam();
            cnpr->conn = accept(listenfd, (struct sockaddr*)&(cnpr->addr), (socklen_t*)&sockaddr_size);
            if(cnpr->conn < 0) {
                Print_line;
            } else {
                pthread_mutex_lock(&mutex_conn);
                ready_conn.push_back(cnpr);
                pthread_mutex_unlock(&mutex_conn);

                pthread_cond_signal(&cond_conn) == 0 ? 0 : Print_line;
                statlog_wr("accept from ip:%s port:%d.",inet_ntoa(cnpr->addr.sin_addr),ntohs(cnpr->addr.sin_port));
            }
        }
        pthread_join(wkpr_recycle.thread,nullptr) == 0 ? 0 : Print_line;
        pthread_join(wkpr_retireHTTP.thread,nullptr) == 0 ? 0 : Print_line;
        pthread_join(wkpr_retireWS.thread,nullptr) == 0 ? 0 : Print_line;
        close(listenfd) == 0 ? 0 : Print_line;
        return;
    }
    /*
     WorkParam listening(){
        WorkParam wkpr_listening;
        wkpr_listening.assign(this,&WT_HT::_listening);
        pthread_create(&(wkpr_listening.thread),&(wkpr_listening.thr_attr),&WT_HT::pthread_adapter,(void*)&wkpr_listening) == 0 ? 0 :Print_line;

    }*/

};

void HttpDT_test() {
    char buff[128] = "GET /home.html HTTP/1.1\r\nHost: developer.mozilla.org\r\n\r\n";
    HttpDT htdt;
    htdt.parse_request(buff,strlen(buff));
    htdt.print_request();
    htdt.parse_clear();
    strncpy(buff,"POST /createUIN HTTP/1.1\r\nHost: developer.mozilla.org\r\nConnection: keep-alive\r\nContent-Length: 5\r\n\r\n12345",128);
    htdt.parse_request(buff,strlen(buff));
    htdt.print_request();
    htdt.parse_clear();
    printf("parse empty request %d close_immediate %d.\n",htdt.parse_request(buff,0),htdt.close_immediate);
}

#if STANDALONE == 1
void index(WT_HT *serv,WorkParam *wkpr) {
    Router::send_same_name_file(wkpr->cnpr,"./chat.html");
}
void echo_ws(WT_HT *serv,WorkParam *wkpr) {
    ;
}
int main() {
    std::string key;
    nice(20);
    WT_HT serv;
    serv.router.root->phase[key.assign("fd")] = new Router_Node();
    serv.router.root->phase[key.assign("fd")].letter = new Router_Node(new Router_Node_DT(Router_Node_DT::ht,Router::send_uri_file));
    serv.router.root->phase[key.assign("index")] = new Router_Node(new Router_Node_DT(Router_Node_DT::ht,Router::send_uri_file));
    serv.router.root->phase[key.assign("ws")] = new Router_Node(new Router_Node_DT(Router_Node_DT::ws,echo_ws));
    serv.listening();
    return 0;
}
#endif
